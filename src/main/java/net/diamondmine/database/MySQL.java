package net.diamondmine.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * MySQL Inherited subclass for making a connection to a MySQL server.
 * 
 * @author PatPeter, Jon la Cour
 */
public class MySQL extends DatabaseHandler {
    private String hostname = "localhost";
    private String portnmbr = "3306";
    private String username = "minecraft";
    private String password = "";
    private String database = "minecraft";

    /**
     * @param log
     *            Logger
     * @param prefix
     *            Logger prefix.
     * @param sqlhostname
     *            MySQL server hostname
     * @param sqlport
     *            MySQL server port
     * @param sqldatabase
     *            MySQL database
     * @param sqluser
     *            MySQL user
     * @param sqlpassword
     *            MySQL password
     */
    public MySQL(final Logger log, final String prefix, final String sqlhostname, final String sqlport, final String sqldatabase, final String sqluser,
            final String sqlpassword) {
        super(log, prefix, " MySQL: ");
        hostname = sqlhostname;
        portnmbr = sqlport;
        database = sqldatabase;
        username = sqluser;
        password = sqlpassword;
    }

    @Override
    public final boolean initialize() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            writeError("Class not found in initialize(): " + e.getMessage() + ".", true);
            return false;
        }
    }

    @Override
    public final Connection open() {
        if (initialize()) {
            String url = "";
            try {
                url = "jdbc:mysql://" + hostname + ":" + portnmbr + "/" + database;
                connection = DriverManager.getConnection(url, username, password);
                return connection;
            } catch (SQLException e) {
                writeError(url, true);
                writeError("SQL exception in open(): " + e.getMessage() + ".", true);
            }
        }
        return connection;
    }

    @Override
    public final void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            writeError("Exception in close(): " + e.getMessage(), true);
        }
    }

    @Override
    public final Connection getConnection() {
        return this.connection;
    }

    @Override
    public final boolean checkConnection() {
        if (connection != null) {
            return true;
        }
        return false;
    }

    @Override
    public final ResultSet query(final String query) {
        Statement statement = null;
        ResultSet result = null;
        try {
            statement = this.connection.createStatement();
            result = statement.executeQuery("SELECT CURTIME()");

            switch (this.getStatement(query)) {
            case SELECT:
                result = statement.executeQuery(query);
                break;

            case INSERT:
            case UPDATE:
            case DELETE:
            case CREATE:
            case ALTER:
            case DROP:
            case TRUNCATE:
            case RENAME:
            case DO:
            case REPLACE:
            case LOAD:
            case HANDLER:
            case CALL:
                lastUpdate = statement.executeUpdate(query);
                break;

            default:
                result = statement.executeQuery(query);
            }
            return result;
        } catch (SQLException e) {
            this.writeError("SQL exception in query(): " + e.getMessage(), false);
        }
        return result;
    }

    @Override
    public final PreparedStatement prepare(final String query) {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(query);
            return ps;
        } catch (SQLException e) {
            if (!e.toString().contains("not return ResultSet")) {
                writeError("SQL exception in prepare(): " + e.getMessage(), false);
            }
        }
        return ps;
    }

    @Override
    public final boolean createTable(final String query) {
        Statement statement = null;
        try {
            if (query == null || query.equals("")) {
                writeError("Parameter 'query' empty or null in createTable(): " + query, true);
                return false;
            }

            statement = connection.createStatement();
            statement.execute(query);
            return true;
        } catch (SQLException e) {
            writeError(e.getMessage(), true);
            return false;
        } catch (Exception e) {
            writeError(e.getMessage(), true);
            return false;
        }
    }

    @Override
    public final boolean checkTable(final String table) {
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM " + table);

            if (result == null) {
                return false;
            }
            if (result != null) {
                return true;
            }
        } catch (SQLException e) {
            if (e.getMessage().contains("exist")) {
                return false;
            } else {
                writeError("SQL exception in checkTable(): " + e.getMessage(), false);
            }
        }

        if (query("SELECT * FROM " + table) == null) {
            return true;
        }
        return false;
    }

    @Override
    public final boolean wipeTable(final String table) {
        Statement statement = null;
        String query = null;
        try {
            if (!checkTable(table)) {
                writeError("Table \"" + table + "\" in wipeTable() does not exist.", true);
                return false;
            }
            statement = this.connection.createStatement();
            query = "DELETE FROM " + table + ";";
            statement.executeUpdate(query);

            return true;
        } catch (SQLException e) {
            if (!e.toString().contains("not return ResultSet")) {
                return false;
            }
        }
        return false;
    }
}
