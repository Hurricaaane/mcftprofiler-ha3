package net.diamondmine.mcftprofiler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Handler for awards.
 * 
 * @author Jon la Cour
 * @version 1.3.1
 */
public class Awards extends McftProfiler {

    /**
     * Send the profile line to the requesting player.
     * 
     * @param name
     *            Who to lookup.
     * @param sender
     *            The requesting player.
     * @since 1.3.0
     */
    public static void send(final String name, final CommandSender sender) {
        fetchAwards(name, sender, false);
    }

    /**
     * Sends a player another player's awards.
     * 
     * @param sender
     *            The command sender.
     * @param args
     *            The command arguments.
     * @param cmdname
     *            The name of the command used.
     * @since 1.2.4
     */
    public static void sendAwards(final CommandSender sender, final String[] args, final String cmdname) {
        boolean mainnode = false;
        Player player = null;
        String pname = null;
        if (sender instanceof Player) {
            player = (Player) sender;
            pname = player.getName();
            if (p.has(player, "mcftprofiler.*")) {
                mainnode = true;
            }
        } else {
            pname = "Console";
            mainnode = true;
        }
        if (mainnode || p.has(player, "mcftprofiler.awards")) {
            if (args.length == 1) {
                String name = "";
                if (args.length == 0) {
                    name = pname;
                } else {
                    name = args[0];
                }
                Player user = Bukkit.getServer().getPlayer(name);
                if (user != null) {
                    name = user.getName();
                }
                fetchAwards(name, sender, true);
            } else {
                fetchAwards(pname, sender, true);
            }
        }
    }

    /**
     * Have a player award another player.
     * 
     * @param sender
     *            The command sender.
     * @param args
     *            Arguments including award name.
     * @param cmdname
     *            The name of the command used.
     * @since 1.2.4
     */
    public static void award(final CommandSender sender, final String[] args, final String cmdname) {
        boolean mainnode = false;
        Player player = null;
        if (sender instanceof Player) {
            player = (Player) sender;
            if (p.has(player, "mcftprofiler.*")) {
                mainnode = true;
            }
        } else {
            mainnode = true;
        }
        if (mainnode || p.has(player, "mcftprofiler.award")) {
            if (args.length < 1) {
                sender.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                sender.sendMessage(ChatColor.RED + "/" + cmdname + " " + ChatColor.GRAY + "name award " + ChatColor.YELLOW + "Gives an award to a user.");
            } else if (args.length == 1) {
                sender.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                sender.sendMessage(ChatColor.RED + "/" + cmdname + " " + args[0] + ChatColor.GRAY + " award" + ChatColor.YELLOW + " Gives an award to "
                        + args[0] + ".");
            } else if (args.length > 1) {
                String name = args[0];
                String award = "";
                for (int i = 1; i < args.length; i++) {
                    String s = args[i];
                    if (award.length() > 0) {
                        award += " ";
                    }
                    award += s;
                }
                giveAward(name, award, sender);
            }
        } else {
            sender.sendMessage(ChatColor.GOLD + "You don't have permission to do that!");
        }
    }

    /**
     * This fetches a players awards.
     * 
     * @param name
     *            The user to fetch awards from
     * @param sender
     *            Command sender
     * @param errorHandling
     *            Whether or not to show error message for no awards
     * @since 1.0.0
     */
    public static void fetchAwards(final String name, final CommandSender sender, final boolean errorHandling) {
        PreparedStatement query = null;
        ResultSet result = null;
        try {
            try {
                query = database.db.prepare("SELECT awards FROM " + database.prefix + "profiles WHERE username LIKE ?");
                query.setString(1, name);
                result = query.executeQuery();

                if (result.next()) {
                    String awards = result.getString("awards");
                    if (awards != null && awards.length() >= 2) {
                        sender.sendMessage(ChatColor.YELLOW + "* " + ChatColor.WHITE + "Awards: " + ChatColor.GRAY + awards);
                    } else {
                        if (errorHandling) {
                            sender.sendMessage(ChatColor.YELLOW + "User has no awards!");
                        }
                    }
                }
            } catch (Exception e) {
                log("Error while fetching awards for user: " + e.getLocalizedMessage(), "warning");
            }
        } finally {
            try {
                query.close();
                result.close();
            } catch (Exception e) {
                log("Error closing queries while fetching awards for user: " + e.getLocalizedMessage(), "severe");
            }
        }
    }

    /**
     * This gives an award to a player.
     * 
     * @param name
     *            The users name that will receive an award
     * @param award
     *            The award
     * @param sender
     *            Command sender
     * @since 1.0.0
     */
    public static void giveAward(final String name, final String award, final CommandSender sender) {
        PreparedStatement query = null;
        ResultSet result = null;
        try {
            query = database.db.prepare("SELECT profileid, username, awards FROM " + database.prefix + "profiles WHERE username LIKE ?");
            query.setString(1, name);
            result = query.executeQuery();

            if (result.next()) {
                String awardslist = result.getString("awards");
                String profileid = result.getString("profileid");
                if (awardslist != null) {
                    String[] awarditem = awardslist.split(", ");
                    for (String s : awarditem) {
                        if (!s.startsWith(award)) {
                            StringBuilder awards = new StringBuilder();
                            if (awarditem.length > 0) {
                                awards.append(awarditem[0]);
                                for (int i = 1; i < awarditem.length; i++) {
                                    awards.append(", ");
                                    awards.append(awarditem[i]);
                                }
                                awards.append(", ").append(award);
                            }
                            awardslist = awards.toString();

                            result.close();
                            PreparedStatement update = database.db.prepare("UPDATE " + database.prefix + "profiles SET awards = ? WHERE profileid = ?");
                            update.setString(1, awardslist);
                            update.setInt(2, Integer.parseInt(profileid));
                            update.executeUpdate();
                            update.close();
                            break;
                        }
                    }
                } else {
                    result.close();
                    PreparedStatement update = database.db.prepare("UPDATE " + database.prefix + "profiles SET awards = ? WHERE profileid = ?");
                    update.setString(1, award);
                    update.setInt(2, Integer.parseInt(profileid));
                    update.executeUpdate();
                    update.close();
                }
            } else {
                result.close();
                PreparedStatement insert = database.db.prepare("INSERT INTO " + database.prefix
                        + "profiles (profileid, username, ip, awards) VALUES (NULL, ?, NULL, ?)");
                insert.setString(1, name);
                insert.setString(2, award);
                insert.executeUpdate();
                insert.close();
            }

            sender.sendMessage(ChatColor.GOLD + name + " has been given the award \"" + award + "\".");
        } catch (Exception e) {
            log("Error while giving award to user: " + e.getLocalizedMessage(), "warning");
        } finally {
            try {
                result.close();
                query.close();
            } catch (Exception e) {
                log("Error closing queries while giving award to user: " + e.getLocalizedMessage(), "severe");
            }
        }
    }

}
