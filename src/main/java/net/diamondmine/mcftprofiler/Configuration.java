package net.diamondmine.mcftprofiler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Handler for settings files.
 * 
 * @author Jon la Cour
 * @version 1.3.0
 */
public class Configuration extends McftProfiler {
    private static FileConfiguration config;

    /**
     * Sets default configuration up if there isn't a configuration file
     * present.
     * 
     * @since 1.3.0
     */
    public static final void checkConfigDefaults() {
        config.options().copyDefaults(true);
        config.addDefault("profile.reputation", true);
        config.addDefault("profile.smart-tracking", true);
        config.addDefault("profile.rank", false);
        config.addDefault("profile.title", true);
        config.addDefault("profile.awards", false);
        config.addDefault("profile.notes.amount-per-page", 5);
        config.addDefault("profile.notes.timezone", "PST");
        config.addDefault("profile.geoip.enabled", false);
        config.addDefault("profile.geoip.file", "/usr/local/share/GeoIP/GeoIP.dat");
        config.addDefault("profile.jail-info.enabled", false);
        config.addDefault("database.type", "sqlite");
        config.addDefault("database.mysql.username", "");
        config.addDefault("database.mysql.password", "");
        config.addDefault("database.mysql.host", "localhost");
        config.addDefault("database.mysql.port", "3306");
        config.addDefault("database.mysql.database", "minecraft");
        config.addDefault("database.mysql.prefix", "");
    }

    /**
     * Reloads the configuration.
     * 
     * @since 1.3.0
     */
    public static final void reloadConfiguration() {
        config = YamlConfiguration.loadConfiguration(new File("plugins/McftProfiler/config.yml"));
        checkConfigDefaults();
    }

    /**
     * Returns the current configuration.
     * 
     * @return FileConfiguration
     * @since 1.3.0
     */
    public static final FileConfiguration getConfiguration() {
        if (config == null) {
            reloadConfiguration();
        }
        return config;
    }

    /**
     * Saves the configuration.
     * 
     * @since 1.3.0
     */
    public static final void saveConfiguration() {
        if (config == null) {
            return;
        }
        try {
            getConfiguration().save(new File("plugins/McftProfiler/config.yml"));
        } catch (IOException e) {
            McftProfiler.log("Could not save configuration: " + e.getLocalizedMessage(), "severe");
        }
    }

    /**
     * Updates a configuration setting.
     * 
     * @param path
     *            YAML path to setting.
     * @param setting
     *            The setting to set at the path.
     * @since 1.3.0
     */
    public static final void updateSetting(final String path, final String setting) {
        config.set(path, setting);
    }

    /**
     * This migrates old settings (pre-1.3.0) to our new configuration file.
     * 
     * @since 1.3.0
     */
    public static void migrateDeprecatedSettings() {
        reloadConfiguration();

        HashMap<String, String> settings = new HashMap<String, String>();
        File configFile = new File("plugins/McftProfiler/settings.txt");
        String line = null;

        if (!configFile.exists()) {
            return;
        }

        try {
            BufferedReader configuration = new BufferedReader(new FileReader(configFile));
            while ((line = configuration.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#") && line.contains(" = ")) {
                    String[] pair = line.split(" = ", 2);
                    settings.put(pair[0], pair[1]);
                }
            }
            configuration.close();
        } catch (Exception e) {
            McftProfiler.log(e.toString() + " error loading deprecated settings: " + e.getMessage(), "severe");
            return;
        }

        if (settings.containsKey("username") && settings.containsKey("password")) {
            McftProfiler.log("Migrating old configuration to new format...");
            if (!settings.get("password").equals("password")) {
                config.set("database.type", "mysql");
                config.set("database.mysql.username", settings.get("username"));
                config.set("database.mysql.password", settings.get("password"));
                config.set("database.mysql.host", settings.get("host"));
                config.set("database.mysql.port", settings.get("port"));
                config.set("database.mysql.database", settings.get("database"));
                if (settings.containsKey("prefix")) {
                    if (!settings.get("prefix").equalsIgnoreCase("off")) {
                        config.set("database.mysql.prefix", settings.get("prefix"));
                    }
                }
            }
            if (!settings.get("ranks").equals("off")) {
                config.set("profile.rank", true);
            }
            if (!settings.get("titles").equals("on")) {
                config.set("profile.title", false);
            }
            if (!settings.get("awards").equals("off")) {
                config.set("profile.rank", true);
            }
            if (!settings.get("max notes").equals("5")) {
                config.set("profile.notes.amount-per-page", Integer.parseInt(settings.get("max notes")));
            }
            if (!settings.get("country").equals("off")) {
                config.set("profile.geoip.enabled", true);
                config.set("profile.geoip.file", settings.get("country database"));
            }
            if (!settings.get("iptracking").equals("on")) {
                config.set("profile.smart-tracking", false);
            }
            if (settings.containsKey("reputation") && !settings.get("reputation").equals("on")) {
                config.set("profile.reputation", false);
            }
            if (!settings.get("jailinfo").equals("off")) {
                config.set("profile.jail-info.enabled", true);
            }
        }

        saveConfiguration();
        configFile.delete();
        McftProfiler.log("Thanks for using McftProfiler, your old configuration has been migrated to our new configuration format.");
    }
}
