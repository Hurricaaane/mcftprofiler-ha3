package net.diamondmine.mcftprofiler;

import java.util.logging.Logger;


import net.diamondmine.database.DatabaseHandler;
import net.diamondmine.database.MySQL;
import net.diamondmine.database.SQLite;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Database handler for MySQL and SQLite.
 * 
 * @author Jon la Cour
 * @since 1.4.0
 */
public class Database {
    public String prefix = "";
    public DatabaseHandler db;

    /**
     * Initializes the database for McftProfiler.
     * 
     * @param plugin
     *            McftProfiler instance
     * @param logger
     *            The Logger for both database types.
     * @param databasePath
     *            The path for the SQLite database.
     * @since 1.3.0
     */
    public Database(final McftProfiler plugin, final Logger logger, final String databasePath) {
        FileConfiguration config = Configuration.getConfiguration();

        // Set database prefix
        prefix = config.getString("database.mysql.prefix", "") + "_";
        if (prefix.equals("_")) {
            prefix = "";
        }

        if (config.get("database.type", "sqlite").equals("mysql")) {
            db = new MySQL(logger, "[McftProfiler]", config.getString("database.mysql.host"), config.getString("database.mysql.port"),
                    config.getString("database.mysql.database"), config.getString("database.mysql.username"), config.getString("database.mysql.password"));

        } else {
            db = new SQLite(logger, "[McftProfiler]", "McftProfiler", databasePath);
        }

        try {
            db.open();
        } catch (Exception e) {
            McftProfiler.log(e.toString() + "exception when connecting to database: " + e.getMessage(), "severe");
        }

        if (db.checkConnection()) {
            checkTables();
        } else {
            McftProfiler.log("Database connection failed.", "severe");
        }

        Bukkit.getServer().getScheduler().runTaskTimerAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                if (!db.checkConnection()) {
                    McftProfiler.log("Unable to connect to database. Attempting to reconnect...", "severe");
                    try {
                        db.open();
                    } catch (Exception e) {
                        McftProfiler.log("We still did not get a response back from the database! We'll retry in 30 seconds.");
                    }
                }
            }
        }, 200L, 600L);
    }

    /**
     * This makes sure that the databases are available. If they aren't it will
     * make them.
     * 
     * @since 1.3.0
     */
    private void checkTables() {
        FileConfiguration config = Configuration.getConfiguration();
        boolean iptracking = config.getBoolean("profile.smart-tracking");
        String error = "exception when updating database: ";
        McftProfiler.log("Connection to the database successful.");
        try {
            String primarykey = "INTEGER PRIMARY KEY AUTOINCREMENT";
            if (db instanceof MySQL) {
                primarykey = "INT NOT NULL AUTO_INCREMENT PRIMARY KEY";
            }

            String notes = "CREATE TABLE " + prefix + "notes (noteid " + primarykey
                    + ", username VARCHAR(32) NOT NULL, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, staff VARCHAR(32) NOT NULL, note VARCHAR(255) NOT NULL)";
            String profiles = "CREATE TABLE "
                    + prefix
                    + "profiles (profileid "
                    + primarykey
                    + ", username VARCHAR(32) NOT NULL, ip VARCHAR(15), laston TIMESTAMP, lastpos VARCHAR(75), awards VARCHAR(255), reputation SMALLINT(6) DEFAULT '0')";
            String iplog = "CREATE TABLE " + prefix + "iplog (ipid " + primarykey + ", ip VARCHAR(15) NOT NULL, users VARCHAR(255) NOT NULL, ips VARCHAR(255))";
            String votelog = "CREATE table " + prefix + "votelog (voteid " + primarykey
                    + ", username VARCHAR(32) NOT NULL, altered VARCHAR(32) NOT NULL, added TINYINT(1) NOT NULL, removed TINYINT(1) NOT NULL)";

            if (db instanceof MySQL) {
                notes += " ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
                profiles += " ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
                iplog += " ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
                votelog += " ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
            }

            if (!db.checkTable(prefix + "notes")) {
                if (iptracking) {
                    McftProfiler.log("Creating database tables '" + prefix + "notes', '" + prefix + "profiles', and '" + prefix + "iplog'.");
                } else {
                    McftProfiler.log("Creating database tables '" + prefix + "notes' and '" + prefix + "profiles'.");
                }
                db.createTable(notes);
            }
            if (!db.checkTable(prefix + "profiles")) {
                db.createTable(profiles);
            }
            if (iptracking) {
                if (!db.checkTable(prefix + "iplog")) {
                    db.createTable(iplog);
                }
            }
            if (!db.checkTable(prefix + "votelog")) {
                db.createTable(votelog);
            }
        } catch (Exception e) {
            McftProfiler.log(e.toString() + error + e.getMessage(), "severe");
        }
    }

}
