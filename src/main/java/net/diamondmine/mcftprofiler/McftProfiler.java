package net.diamondmine.mcftprofiler;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import net.diamondmine.mcftprofiler.listeners.Listeners;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of McftProfiler.
 * 
 * @author Jon la Cour
 * @version 1.4.0
 */
public class McftProfiler extends JavaPlugin {
    public final static Logger logger = Bukkit.getLogger();
    public static HashMap<String, String> seeds = new HashMap<String, String>();
    public static Permission p = null;
    public static Chat c = null;
    public static Database database;

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        log("Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        // Configuration
        Configuration.migrateDeprecatedSettings();

        // Vault permissions API
        if (!setupPermissions()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        
        // Vault chat API
        setupChat();

        database = new Database(this, logger, getDataFolder().getAbsolutePath());

        List<World> worlds = getServer().getWorlds();
        for (World world : worlds) {
            seeds.put(world.getName(), Long.toString(world.getSeed()));
        }

        getServer().getPluginManager().registerEvents(new Listeners(), this);
        log("Version " + getDescription().getVersion() + " enabled");
    }

    @Override
    public final boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, final String[] args) {
        String cmdname = cmd.getName().toLowerCase();
        if (cmdname.equals("award")) {
            Awards.award(sender, args, cmdname);
        }
        if (cmdname.equals("awards")) {
            Awards.sendAwards(sender, args, cmdname);
        }
        if (cmdname.equals("dislike")) {
            Reputation.dislike(sender, args, cmdname);
        }
        if (cmdname.equals("like")) {
            Reputation.like(sender, args, cmdname);
        }
        if (cmdname.equals("note")) {
            Notes.note(sender, args, cmdname);
        }
        if (cmdname.equals("reputation")) {
            Reputation.sendReputation(sender, args, cmdname);
        }
        if (cmdname.equals("status") || cmdname.equals("profile")) {
            Profile.sendProfile(sender, args, cmdname);
        }
        if (cmdname.equals("tpoff") || cmdname.equals("lastpos")) {
            Profile.gotoLastPos(sender, args, cmdname);
        }
        return false;
    }

    /**
     * Checks to make sure a permissions plugin has been loaded.
     * 
     * @return True if one is present, otherwise false.
     * @since 1.0.5
     */
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(
                net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            p = permissionProvider.getProvider();
        }
        return (p != null);
    }
    
    /**
     * Checks to make sure a chat plugin has been loaded.
     * 
     * @return True if one is present, otherwise false.
     * @since 1.4.0
     */
    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            c = chatProvider.getProvider();
        }

        return (c != null);
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.3
     */
    public static void log(final String s, final String type) {
        String message = "[McftProfiler] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send.
     */
    public static void log(final String s) {
        log(s, "info");
    }
}
