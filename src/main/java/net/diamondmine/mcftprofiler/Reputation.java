package net.diamondmine.mcftprofiler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Handler for profiles and related accounts.
 * 
 * @author Jon la Cour
 * @version 1.3.1
 */
public class Reputation extends McftProfiler {

    /**
     * Send a player another player's reputation.
     * 
     * @param sender
     *            The command sender.
     * @param args
     *            The command arguments.
     * @param cmdname
     *            The name of the command used.
     * @since 1.2.4
     */
    public static void sendReputation(final CommandSender sender, final String[] args, final String cmdname) {
        boolean mainnode = false, reputationnode = false;
        Player player = null;
        String pname = null;
        if (sender instanceof Player) {
            player = (Player) sender;
            pname = player.getName();
            if (p.has(player, "mcftprofiler.*")) {
                mainnode = true;
            } else if (p.has(player, "mcftprofiler.reputation.*")) {
                reputationnode = true;
            }
        } else {
            pname = "Console";
            mainnode = true;
        }
        if (mainnode || reputationnode || p.has(player, "mcftprofiler.reputation.alter")) {
            if (args.length == 2) {
                String name = args[1];
                if (Bukkit.getServer().getPlayer(name) != null) {
                    Player user = Bukkit.getServer().getPlayer(name);
                    name = user.getName();
                }
                if (args[0].equals("add")) {
                    Reputation.addReputation(name, pname, sender);
                } else if (args[0].equals("remove")) {
                    Reputation.removeReputation(name, pname, sender);
                } else {
                    sender.sendMessage(ChatColor.GOLD + "Please use add or remove to change a user's reputation.");
                }
            } else {
                sender.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                sender.sendMessage(ChatColor.RED + "/" + cmdname + " " + ChatColor.GRAY + "add/remove name " + ChatColor.YELLOW
                        + "Adds or removes reputation from a user.");
            }
        }
    }

    /**
     * Have a player dislike another.
     * 
     * @param sender
     *            The command sender.
     * @param args
     *            Arguments including who to dislike.
     * @param cmdname
     *            The name of the command used.
     * @since 1.2.4
     */
    public static void dislike(final CommandSender sender, final String[] args, final String cmdname) {
        boolean mainnode = false, reputationnode = false;
        Player player = null;
        String pname = null;
        if (sender instanceof Player) {
            player = (Player) sender;
            pname = player.getName();
            if (p.has(player, "mcftprofiler.*")) {
                mainnode = true;
            } else if (p.has(player, "mcftprofiler.reputation.*")) {
                reputationnode = true;
            }
        } else {
            pname = "Console";
            mainnode = true;
        }
        if (mainnode || reputationnode || p.has(player, "mcftprofiler.reputation.alter")) {
            if (args.length == 1) {
                String name = args[0];
                if (Bukkit.getServer().getPlayer(name) != null) {
                    Player user = Bukkit.getServer().getPlayer(name);
                    name = user.getName();
                }
                Reputation.removeReputation(name, pname, sender);
            } else {
                sender.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                sender.sendMessage(ChatColor.RED + "/" + cmdname + " " + ChatColor.GRAY + "name " + ChatColor.YELLOW + "Removes reputation from a user.");
            }
        }
    }

    /**
     * Have a player like another.
     * 
     * @param sender
     *            The command sender.
     * @param args
     *            Arguments including who to like.
     * @param cmdname
     *            The name of the command used.
     * @since 1.2.4
     */
    public static void like(final CommandSender sender, final String[] args, final String cmdname) {
        boolean mainnode = false, reputationnode = false;
        Player player = null;
        String pname = null;
        if (sender instanceof Player) {
            player = (Player) sender;
            pname = player.getName();
            if (p.has(player, "mcftprofiler.*")) {
                mainnode = true;
            } else if (p.has(player, "mcftprofiler.reputation.*")) {
                reputationnode = true;
            }
        } else {
            pname = "Console";
            mainnode = true;
        }
        if (mainnode || reputationnode || p.has(player, "mcftprofiler.reputation.alter")) {
            if (args.length == 1) {
                String name = args[0];
                if (Bukkit.getServer().getPlayer(name) != null) {
                    Player user = Bukkit.getServer().getPlayer(name);
                    name = user.getName();
                }
                addReputation(name, pname, sender);
            } else {
                sender.sendMessage(ChatColor.GOLD + "Not enough arguments!");
                sender.sendMessage(ChatColor.RED + "/" + cmdname + " " + ChatColor.GRAY + "name " + ChatColor.YELLOW + "Adds reputation to a user.");
            }
        }
    }

    public static final int HIGH_REP_SCORE = 5;
    public static final int LOW_REP_SCORE = -5;

    /**
     * This adds reputation to a user.
     * 
     * @param name
     *            The users name to add reputation to
     * @param pname
     *            The players name
     * @param sender
     *            Command sender
     * @since 1.1.3
     */
    public static void addReputation(final String name, final String pname, final CommandSender sender) {
        try {
            ResultSet voted = database.db.query("SELECT voteid, added, removed FROM " + database.prefix + "votelog WHERE username = '" + pname
                    + "' AND altered = '" + name + "' LIMIT 10;");
            boolean vrows = voted.next();
            if (vrows) {
                String voteid = voted.getString("voteid");
                if (voted.getInt("added") == 1) {
                    sender.sendMessage(ChatColor.GOLD + "Sorry you've already liked this user.");
                } else if (voted.getInt("removed") == 1) {
                    sender.sendMessage(ChatColor.GOLD + "You've already disliked this user. Your vote has been changed in their favor.");
                    voted.close();
                    ResultSet profile = database.db.query("SELECT profileid, username, reputation FROM " + database.prefix + "profiles WHERE username = '"
                            + name + "' LIMIT 10;");
                    boolean prows = profile.next();
                    if (prows) {
                        String profileid = profile.getString("profileid");
                        int reputation = profile.getInt("reputation") + 2;
                        profile.close();
                        database.db.query("UPDATE " + database.prefix + "profiles SET reputation = " + reputation + " WHERE profileid = " + profileid + ";");
                    }
                    profile.close();
                }
                voted.close();
                database.db.query("UPDATE " + database.prefix + "votelog SET added = '1', removed = '0' WHERE voteid = " + voteid + ";");
            } else {
                voted.close();
                ResultSet profile = database.db.query("SELECT profileid, username, reputation FROM " + database.prefix + "profiles WHERE username = '" + name
                        + "' LIMIT 10;");
                boolean prows = profile.next();
                if (prows) {
                    String profileid = profile.getString("profileid");
                    int reputation = profile.getInt("reputation") + 1;
                    profile.close();
                    database.db.query("UPDATE " + database.prefix + "profiles SET reputation = " + reputation + " WHERE profileid = " + profileid + ";");
                    database.db.query("INSERT INTO " + database.prefix + "votelog (username, altered, added, removed) VALUES ('" + pname + "', '" + name
                            + "', '1', '0');");
                    sender.sendMessage(ChatColor.GOLD + "You've liked " + name + ".");
                } else {
                    profile.close();
                    sender.sendMessage(ChatColor.GOLD + "Sorry, we don't know that user. You entered " + name + ".");
                }
            }
        } catch (Exception e) {
            log("Error while adding to users reputation: " + e.getLocalizedMessage(), "warning");
        }
    }

    /**
     * This removes reputation from a user.
     * 
     * @param name
     *            The users name to remove reputation from
     * @param pname
     *            The players name
     * @param sender
     *            Command sender
     * @since 1.1.3
     */
    public static void removeReputation(final String name, final String pname, final CommandSender sender) {
        try {
            ResultSet voted = database.db.query("SELECT voteid, added, removed FROM " + database.prefix + "votelog WHERE username = '" + pname
                    + "' AND altered = '" + name + "' LIMIT 10;");
            boolean vrows = voted.next();
            if (vrows) {
                String voteid = voted.getString("voteid");
                if (voted.getInt("added") == 1) {
                    voted.close();
                    sender.sendMessage(ChatColor.GOLD + "You've already liked for this user. Your vote has been changed against them.");
                    ResultSet profile = database.db.query("SELECT profileid, username, reputation FROM " + database.prefix + "profiles WHERE username = '"
                            + name + "' LIMIT 10;");
                    boolean prows = profile.next();
                    if (prows) {
                        String profileid = profile.getString("profileid");
                        int reputation = profile.getInt("reputation") - 2;
                        profile.close();
                        database.db.query("UPDATE " + database.prefix + "profiles SET reputation = " + reputation + " WHERE profileid = " + profileid + ";");
                    }
                    profile.close();
                } else if (voted.getInt("removed") == 1) {
                    sender.sendMessage(ChatColor.GOLD + "Sorry you've already voted against this user.");
                }
                voted.close();
                database.db.query("UPDATE " + database.prefix + "votelog SET added = '0', removed = '1' WHERE voteid = " + voteid + ";");
            } else {
                voted.close();
                ResultSet profile = database.db.query("SELECT profileid, username, reputation FROM " + database.prefix + "profiles WHERE username = '" + name
                        + "' LIMIT 10;");
                boolean prows = profile.next();
                if (prows) {
                    String profileid = profile.getString("profileid");
                    int reputation = profile.getInt("reputation") - 1;
                    database.db.query("UPDATE " + database.prefix + "profiles SET reputation = " + reputation + " WHERE profileid = " + profileid + ";");
                    database.db.query("INSERT INTO " + database.prefix + "votelog (username, altered, added, removed) VALUES ('" + pname + "', '" + name
                            + "', '0', '1');");
                    sender.sendMessage(ChatColor.GOLD + "You've disliked " + name + ".");
                    profile.close();
                } else {
                    profile.close();
                    sender.sendMessage(ChatColor.GOLD + "Sorry, we don't know that user. You entered " + name + ".");
                }
            }
        } catch (Exception e) {
            log("Error while removing users reputation: " + e.getLocalizedMessage(), "warning");
        }
    }

    /**
     * This fetches a users reputation score.
     * 
     * @param name
     *            The user to fetch reputation for
     * @return The users reputation score.
     * @since 1.1.3
     */
    public static String getReputation(final String name) {
        PreparedStatement query = null;
        ResultSet result = null;
        try {
            query = database.db.prepare("SELECT reputation FROM " + database.prefix + "profiles WHERE username = ?");
            query.setString(1, name);
            result = query.executeQuery();

            String score = "";

            if (result.next()) {
                do {
                    try {
                        score = "";
                        int rep = result.getInt("reputation");
                        if (rep > HIGH_REP_SCORE) {
                            score += ChatColor.DARK_GREEN;
                        } else if (rep > 0) {
                            score += ChatColor.GREEN;
                        } else if (rep == 0) {
                            score += ChatColor.GRAY;
                        } else if (rep < 0) {
                            score += ChatColor.RED;
                        } else if (rep < LOW_REP_SCORE) {
                            score += ChatColor.DARK_RED;
                        }
                        score += Integer.toString(rep);
                    } catch (Exception e) {
                        score = ChatColor.GRAY + "0";
                    }
                } while (result.next());
            } else {
                return ChatColor.GRAY + "0";
            }

            return score;
        } catch (Exception e) {
            log("Error while fetching users reputation: " + e.getLocalizedMessage(), "warning");
        } finally {
            try {
                query.close();
                result.close();
            } catch (Exception e) {
                log("Error closing queries while fetching users reputation: " + e.getLocalizedMessage(), "severe");
            }
        }

        return ChatColor.GRAY + "0";
    }

}
