package net.diamondmine.mcftprofiler.listeners;

import static net.diamondmine.mcftprofiler.Util.fixColor;

import java.sql.Timestamp;
import java.util.GregorianCalendar;

import net.diamondmine.mcftprofiler.IPHandler;
import net.diamondmine.mcftprofiler.LastPosition;
import net.diamondmine.mcftprofiler.McftProfiler;
import net.diamondmine.mcftprofiler.Profile;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.WorldLoadEvent;

/**
 * Handler for all player related events.
 * 
 * @author Jon la Cour
 * @version 1.4.0
 */
public class Listeners implements Listener {
    private static final int RESERVED_SPACE = 13;

    /**
     * Player join tasks.
     * 
     * @param event
     *            PlayerJoinEvent
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public final void onPlayerJoin(final PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String ipaddress = player.getAddress().toString().substring(1).split(":")[0];
        if (ipaddress.contains("/")) {
            ipaddress = ipaddress.split("/")[1];
        }
        String name = player.getName();
        IPHandler.logIP(ipaddress, name);
        Profile.checkProfile(ipaddress, name);

        String related = Profile.relatedAccounts(name, player);
        
        World world = Bukkit.getServer().getWorlds().get(0);
        String rank = ChatColor.WHITE + " " + McftProfiler.c.getPrimaryGroup(world, name);
        String prefix = fixColor(McftProfiler.c.getGroupPrefix(world, rank));
        
        String notification = prefix + name + " " + ChatColor.RED + "may be: " + related;

        if (!McftProfiler.p.has(player, "mcftprofiler.related.hide") && !related.isEmpty()) {
            boolean sent = false;
            for (Player players : Bukkit.getServer().getOnlinePlayers()) {
                if (McftProfiler.p.has(players, "mcftprofiler.related.notify")) {
                    players.sendMessage(notification);
                    sent = true;
                }
                if (!sent && McftProfiler.p.has(players, "mcftprofiler.related.*")) {
                    players.sendMessage(notification);
                    sent = true;
                }
                if (!sent && McftProfiler.p.has(players, "mcftprofiler.*")) {
                    players.sendMessage(notification);
                }
            }
        }
        try {
            Timestamp timestamp = new Timestamp(new GregorianCalendar().getTimeInMillis());
            McftProfiler.database.db.query("UPDATE " + McftProfiler.database.prefix + "profiles SET laston = '" + timestamp + "' WHERE username = '" + name
                    + "';");
        } catch (Exception e) {
            McftProfiler.log("Error processing player join event: " + e.getLocalizedMessage(), "warning");
        }
    }

    /**
     * Player quit tasks.
     * 
     * @param event
     *            PlayerQuitEvent
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public final void onPlayerQuit(final PlayerQuitEvent event) {
        String error = "exception when logging player last on time: ";
        Player player = event.getPlayer();
        try {
            Timestamp timestamp = new Timestamp(new GregorianCalendar().getTimeInMillis());
            McftProfiler.database.db.query("UPDATE " + McftProfiler.database.prefix + "profiles SET laston = '" + timestamp + "' WHERE username = '"
                    + player.getName() + "';");
        } catch (Exception e) {
            McftProfiler.log(e.toString() + error + e.getMessage(), "severe");
        }
        Location location = player.getLocation();
        LastPosition.logLastPos(player.getName(), location.getX(), location.getY(), location.getZ(), location.getWorld());
    }

    /**
     * Adds seed of newly loaded worlds for player position logging purposes.
     * 
     * @param event
     *            WorldLoadEvent
     */
    @EventHandler(priority = EventPriority.LOW)
    public final void onWorldLoad(final WorldLoadEvent event) {
        if (McftProfiler.seeds.containsKey(event.getWorld().getName())) {
            McftProfiler.seeds.remove(event.getWorld().getName());
        }
        String seed = Long.toString(event.getWorld().getSeed());
        int maxLength = (seed.length() < RESERVED_SPACE) ? seed.length() : RESERVED_SPACE;
        seed = seed.substring(0, maxLength);
        McftProfiler.seeds.put(event.getWorld().getName(), seed);
    }
}
